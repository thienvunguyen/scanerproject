//
//  Constants.swift
//  ScanerProject
//
//  Created by Vu Thien on 8/8/22.
//

import Foundation

struct Constants {
    static var couponCode =  ""
    static var room =  ""
    static var prefix = "+;’c/b-0n"
    static var accepted = false
}
