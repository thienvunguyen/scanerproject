//
//  DetailViewController.swift
//  ScanerProject
//
//  Created by Vu Thien on 8/8/22.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var coupon: UITextView!
    @IBOutlet weak var room: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coupon.text = Constants.couponCode
        room.text = Constants.room

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
