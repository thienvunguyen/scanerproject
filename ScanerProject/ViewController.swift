//
//  ViewController.swift
//  ScanerProject
//
//  Created by Vu Thien on 8/8/22.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, UITextFieldDelegate, AVCaptureMetadataOutputObjectsDelegate {
    var timer = Timer()
    private var timeoutInSeconds: TimeInterval {
        // 5 minutes
        return 2
    }
    @IBOutlet weak var textFieldFirstResponse: UITextField!
    
    @IBOutlet weak var textFieldResponse2: UITextField!
    @IBOutlet weak var preFixTextField: UITextField!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    @IBOutlet weak var cameraScan: UIImageView!
    var qrCodeFrameView: UIView?
    var firstLoad = true
    var breakOut = false
    var stringTotal = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // this is for scan QR by using camera
        readyScanQRCode()
    }
    
    @objc func defineTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: timeoutInSeconds, target: self, selector: #selector(ViewController.doStuff), userInfo: nil, repeats: true)
    }

    @IBAction func validatePrefix(_ sender: Any) {
        if(preFixTextField.text == Constants.prefix) {
            Constants.accepted = true
            let ac = UIAlertController(title: "Validation Completed", message: "", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }

    override func pressesBegan(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        if(Constants.accepted == true) {
            if(firstLoad == true) {
                self.defineTimer()
                self.firstLoad = false
                textFieldResponse2.text = ""
                textFieldFirstResponse.text = ""
            }
            guard let key = presses.first?.key else { return }
            if(key.characters == ";") {
                stringTotal = key.characters
                return;
            }

            if(stringTotal.range(of:";") != nil) {
                stringTotal = stringTotal+String(key.characters)
                let fullNameArr = stringTotal.components(separatedBy: ";")
                textFieldResponse2.text = String(fullNameArr[1])
            } else {
                stringTotal = stringTotal+String(key.characters)
                textFieldFirstResponse.text = stringTotal
            }
        }
    }
    
    @objc func doStuff() {
          // perform any action you wish to
        self.timer.invalidate()
        let next = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    @objc func resetTimer() {
        self.timer.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: timeoutInSeconds, target: self, selector: #selector(ViewController.doStuff), userInfo: nil, repeats: true)
    }

    override func pressesEnded(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        Constants.couponCode = textFieldFirstResponse.text!
        Constants.room = textFieldResponse2.text!

    }
    
    override func pressesChanged(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        print("hmm")
    }
    
    
    func readyScanQRCode() {
        captureSession = AVCaptureSession()
        qrCodeFrameView = UIView()
        guard let videoCaptureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = self.cameraScan.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        
        // check rotate of ipad
        let app = UIApplication.shared
        switch app.statusBarOrientation {
        case .portrait:
            previewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait;
        case .portraitUpsideDown:
            previewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portraitUpsideDown;
        case .landscapeLeft:
            previewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeLeft;
        case .landscapeRight:
            previewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight;
        default:
            previewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight;
        }
        self.cameraScan.layer.addSublayer(previewLayer)

        // add view detect qr code
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            self.cameraScan.addSubview(qrCodeFrameView)
            self.cameraScan.bringSubviewToFront(qrCodeFrameView)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        qrCodeFrameView?.frame = CGRect.zero
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.breakOut = false
        self.firstLoad = true
        self.stringTotal = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
        self.timer.invalidate()
    }
    
    @IBAction func getData(_ sender: Any) {
      print(sender)
    }

    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }

        dismiss(animated: true)
    }

    func found(code: String) {
        let fullNameArr = code.components(separatedBy: ";")
        let group = DispatchGroup()
        for text in fullNameArr {
            group.enter()
            if(breakOut == false) {
                textFieldFirstResponse.text = text
                Constants.couponCode = textFieldFirstResponse.text!
                breakOut = true
            } else {
                textFieldResponse2.text = text
                Constants.room = textFieldResponse2.text!
            }
            group.leave()
        }
        
        group.notify(queue: DispatchQueue.main) {
            let next = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            self.navigationController?.pushViewController(next, animated: true)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    @IBAction func submitaction(_ sender: Any) {
        if(!textFieldFirstResponse.text!.isEmpty && !textFieldResponse2.text!.isEmpty) {
            Constants.couponCode = textFieldFirstResponse.text!
            Constants.room = textFieldResponse2.text!
            let next = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            self.navigationController?.pushViewController(next, animated: true)
        } else {
            let ac = UIAlertController(title: "Need parameter", message: "coupon code or room empty", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

